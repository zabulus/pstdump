﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;
using System.Reflection;
using System.IO;
using System.Text;

namespace PSTReader
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            string sourceDir = args[i++];
            string destPath = args[i++];
            string[] sourceFiles = Directory.EnumerateFiles(sourceDir).ToArray();
            foreach (string sourceFile in sourceFiles)
            {
                string DestFile = Path.GetFileName(sourceFile);
                DestFile = Path.ChangeExtension(DestFile, ".txt");
                DumpToFile(sourceFile, Path.Combine(sourceDir, DestFile));
            }
        }

        private static void DumpToFile(string sourceFile, string destPath)
        {

            try
            {
                IEnumerable<MailItem> mailItems = readPst<MailItem>(sourceFile);
                mailItems = mailItems.OrderBy(c => c.Subject).ThenBy(c => c.CreationTime);
                IEnumerable<ContactItem> contactItems = readPst<ContactItem>(sourceFile);
                contactItems = contactItems.OrderBy(c => c.Subject).ThenByDescending(c => c.CreationTime);
                IEnumerable<TaskItem> taskItems = readPst<TaskItem>(sourceFile);
                taskItems = taskItems.OrderBy(c => c.Subject).ThenByDescending(c => c.CreationTime);
                IEnumerable<AppointmentItem> appointmentItems = readPst<AppointmentItem>(sourceFile);
                appointmentItems = appointmentItems.OrderBy(c => c.Subject).ThenByDescending(c => c.CreationTime);

                using (var file = File.OpenWrite(destPath))
                {
                    using (var writer = new StreamWriter(file, Encoding.Unicode))
                    {
                        foreach (MailItem mailItem in mailItems)
                        {
                            writer.WriteLine("Alternate Recipient Allowed" + mailItem.AlternateRecipientAllowed);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                            writer.WriteLine("Subject" + mailItem.Subject);
                        }
                        writer.WriteLine("");
                        writer.WriteLine("Appointments");
                        writer.WriteLine("");
                        foreach (AppointmentItem appointmentItem in appointmentItems)
                        {
                            writer.WriteLine("All Day Event: " + appointmentItem.AllDayEvent);
                        }
                        writer.WriteLine("");
                        writer.WriteLine("Tasks");
                        writer.WriteLine("");
                        foreach (TaskItem taskItem in taskItems)
                        {
                            writer.WriteLine("Actions:" + taskItem.ActualWork);
                            //writer.WriteLine(": " + taskItem.Attachments);
                            writer.WriteLine("Billing Information: " + taskItem.BillingInformation);
                            writer.WriteLine("Body: " + taskItem.Body);
                            writer.WriteLine("Categories: " + taskItem.Categories);
                            writer.WriteLine("Class: " + taskItem.Class);
                            writer.WriteLine("Companies: " + taskItem.Companies);
                            writer.WriteLine("Complete: " + taskItem.Complete);
                            writer.WriteLine("CreationTime: " + taskItem.CreationTime);
                            writer.WriteLine("Date Completed: " + taskItem.DateCompleted);
                            writer.WriteLine("Delegation State: " + taskItem.DelegationState);
                            writer.WriteLine("Delegator: " + taskItem.Delegator);
                            writer.WriteLine("Due Date: " + taskItem.DueDate);
                            writer.WriteLine("EntryID " + taskItem.EntryID);
                            writer.WriteLine("FormDescription: " + taskItem.FormDescription);
                            writer.WriteLine("Importance: " + taskItem.Importance);
                            writer.WriteLine("Is Recurring: " + taskItem.IsRecurring);
                            writer.WriteLine("Message Clas " + taskItem.MessageClass);
                            writer.WriteLine("Mileage: " + taskItem.Mileage);
                            writer.WriteLine("Ordinal: " + taskItem.Ordinal);
                            writer.WriteLine("Owner: " + taskItem.Owner);
                            writer.WriteLine("Ownership: " + taskItem.Ownership);
                            writer.WriteLine("PercentComplete: " + taskItem.PercentComplete);
                            writer.WriteLine("Recipients: " + taskItem.Recipients);
                            writer.WriteLine("Reminder Override Default: " + taskItem.ReminderOverrideDefault);
                            writer.WriteLine("Reminder Play Sound: " + taskItem.ReminderPlaySound);
                            writer.WriteLine("Recipients: " + taskItem.Recipients);
                            writer.WriteLine("Reminder Set: " + taskItem.ReminderSet);
                            writer.WriteLine("Reminder Sound File: " + taskItem.ReminderSoundFile);
                            writer.WriteLine("Reminder Time): " + taskItem.ReminderTime);
                            writer.WriteLine("Response State: " + taskItem.ResponseState);
                            writer.WriteLine("Role: " + taskItem.Role);
                            writer.WriteLine("RTF Body: " + taskItem.RTFBody);
                            writer.WriteLine("Send Using Account: " + taskItem.SendUsingAccount);
                            writer.WriteLine("Sensitivity: " + taskItem.Sensitivity);
                            writer.WriteLine("Start Date: " + taskItem.StartDate);
                            writer.WriteLine("Status: " + taskItem.Status);
                            writer.WriteLine("Status On Completion Recipients: " + taskItem.StatusOnCompletionRecipients);
                            writer.WriteLine("Subject: " + taskItem.Subject);
                            writer.WriteLine("Team Task: " + taskItem.TeamTask);
                            writer.WriteLine("Tota lWork): " + taskItem.TotalWork);




                        }
                        writer.WriteLine("");
                        writer.WriteLine("Contacts");
                        writer.WriteLine("");
                        // var props = GetPublicProperties(typeof(ContactItem));
                        foreach (ContactItem contactItem in contactItems)
                        {
                            dynamic dyn = (dynamic)contactItem;
                            writer.WriteLine("Last Name: " + contactItem.LastName);
                            writer.WriteLine("FirstName: " + contactItem.FirstName);
                            writer.WriteLine("Middle Name: " + contactItem.MiddleName);
                            writer.WriteLine("Home Addres: " + contactItem.HomeAddress);
                            writer.WriteLine("Home Address City :" + contactItem.HomeAddressCity);
                            writer.WriteLine("Home Address Country: " + contactItem.HomeAddressCountry);
                            writer.WriteLine("Home Address PostalCode: " + contactItem.HomeAddressPostalCode);
                            writer.WriteLine("Home Address PostOffice Box :" + contactItem.HomeAddressPostOfficeBox);
                            writer.WriteLine("Home Address State: " + contactItem.HomeAddressState);
                            writer.WriteLine("Home Address Street: " + contactItem.HomeAddressStreet);
                            writer.WriteLine("Home Fax Number: " + contactItem.HomeFaxNumber);
                            writer.WriteLine("Home Telephone Number" + contactItem.HomeTelephoneNumber);
                            writer.WriteLine("IM Address: " + contactItem.IMAddress);
                            writer.WriteLine("Importance: " + contactItem.Importance);
                            writer.WriteLine("JobTitle: " + contactItem.JobTitle);
                            writer.WriteLine("Mailing Address: " + contactItem.MailingAddress);
                            writer.WriteLine("Account: " + contactItem.Account);
                            writer.WriteLine("Anniversary: " + contactItem.Anniversary);
                            writer.WriteLine("Assistant Name: " + contactItem.AssistantName);
                            writer.WriteLine("Assistant Telephone Number: " + contactItem.AssistantTelephoneNumber);
                            //foreach(var attach in contactItem.OfType<Attachment>())
                            //{
                            //    writer.WriteLine("Attach DisplayName: " + attach.DisplayName);
                            //}
                            writer.WriteLine("Billing Information): " + contactItem.BillingInformation);
                            writer.WriteLine("Birthday: " + contactItem.Birthday);
                            writer.WriteLine("Body: " + contactItem.Body);
                            writer.WriteLine("Business2 Telephone Number:" + contactItem.Business2TelephoneNumber);
                            writer.WriteLine("Business Address: " + contactItem.BusinessAddress);
                            writer.WriteLine("Business Address City:" + contactItem.BusinessAddressCity);
                            writer.WriteLine("Business Address Country: " + contactItem.BusinessAddressCountry);
                            writer.WriteLine("Business Address Postal Code: " + contactItem.BusinessAddressPostalCode);
                            writer.WriteLine("Business Address PostOffice Box: " + contactItem.BusinessAddressPostOfficeBox);
                            writer.WriteLine("Business Address State: " + contactItem.BusinessAddressState);
                            writer.WriteLine("Business Address Street:" + contactItem.BusinessAddressStreet);
                            writer.WriteLine("Business Card Type: " + contactItem.BusinessCardType);
                            writer.WriteLine("Business Fax Number: " + contactItem.BusinessFaxNumber);
                            writer.WriteLine("Business Home Page: " + contactItem.BusinessHomePage);
                            writer.WriteLine("Business Telephone Number: " + contactItem.BusinessTelephoneNumber);
                            writer.WriteLine("Callback Telephone Number: " + contactItem.CallbackTelephoneNumber);
                            writer.WriteLine("Car Telephone Number: " + contactItem.CarTelephoneNumber);
                            writer.WriteLine("Categories: " + contactItem.Categories);
                            writer.WriteLine("Children: " + contactItem.Children);
                            writer.WriteLine("Class: " + contactItem.Class);
                            writer.WriteLine("Company Main Telephone Number: " + contactItem.CompanyMainTelephoneNumber);
                            writer.WriteLine("Company Name: " + contactItem.CompanyName);
                            writer.WriteLine("CreationTime: " + contactItem.CreationTime);
                            writer.WriteLine("Department: " + contactItem.Department);
                            writer.WriteLine("Email1 Address: " + contactItem.Email1Address);
                            writer.WriteLine("Email1 Address Type): " + contactItem.Email1AddressType);
                            writer.WriteLine("Email1 Display Name: " + contactItem.Email1DisplayName);
                            writer.WriteLine("Email2 Address: " + contactItem.Email2Address);
                            writer.WriteLine("Email2 Address Type: " + contactItem.Email2AddressType);
                            writer.WriteLine("Email2 Display Name: " + contactItem.Email2DisplayName);
                            writer.WriteLine("Email3 Address: " + contactItem.Email3Address);
                            writer.WriteLine("Email3 Address Type: " + contactItem.Email3AddressType);
                            writer.WriteLine("Email3 Display Name: " + contactItem.Email3DisplayName);
                            writer.WriteLine("Full Name: " + contactItem.FullName);
                            writer.WriteLine("Gender: " + contactItem.Gender);
                            writer.WriteLine("Hobby: " + contactItem.Hobby);
                            writer.WriteLine("Mailing Address City: " + contactItem.MailingAddressCity);
                            writer.WriteLine("Mailing Address Country: " + contactItem.MailingAddressCountry);
                            writer.WriteLine("Mailing Address PostalCode: " + contactItem.MailingAddressPostalCode);
                            writer.WriteLine("Mailing Address PostOffice Box): " + contactItem.MailingAddressPostOfficeBox);
                            writer.WriteLine("Mailing Address State: " + contactItem.MailingAddressState);
                            writer.WriteLine("Mailing Address Street: " + contactItem.MailingAddressStreet);
                            writer.WriteLine("Manager Name: " + contactItem.ManagerName);
                            writer.WriteLine("Message Class: " + contactItem.MessageClass);
                            writer.WriteLine("Mileage: " + contactItem.Mileage);
                            writer.WriteLine("Mobile Telephone Number: " + contactItem.MobileTelephoneNumber);
                            writer.WriteLine("Nick Name: " + contactItem.NickName);
                            writer.WriteLine("Office Location: " + contactItem.OfficeLocation);
                            writer.WriteLine("Other Address: " + contactItem.OtherAddress);
                            writer.WriteLine("Other Address City: " + contactItem.OtherAddressCity);
                            writer.WriteLine("Other Address Country: " + contactItem.OtherAddressCountry);
                            writer.WriteLine("Other Address PostalCode: " + contactItem.OtherAddressPostalCode);
                            writer.WriteLine("Other Address PostOffice Box: " + contactItem.OtherAddressPostOfficeBox);
                            writer.WriteLine("Other Address State: " + contactItem.OtherAddressState);
                            writer.WriteLine("Other Address Street: " + contactItem.OtherAddressStreet);
                            writer.WriteLine("Other Fax Number: " + contactItem.OtherFaxNumber);
                            writer.WriteLine("Other Telephone Number: " + contactItem.OtherTelephoneNumber);
                            writer.WriteLine("Personal Home Page: " + contactItem.PersonalHomePage);
                            writer.WriteLine("Primary Telephone Number: " + contactItem.PrimaryTelephoneNumber);
                            writer.WriteLine("Profession: " + contactItem.Profession);
                            //writer.WriteLine("Read: " + contactItem.Read);
                            writer.WriteLine("RTFBody: " + contactItem.RTFBody);
                            writer.WriteLine("Sensitivity: " + contactItem.Sensitivity);
                            writer.WriteLine("Subject: " + contactItem.Subject);
                            writer.WriteLine("Title: " + contactItem.Title);
                            writer.WriteLine("TTYTDD Telephone Number: " + contactItem.TTYTDDTelephoneNumber);
                            writer.WriteLine("UnRead: " + contactItem.UnRead);
                            writer.WriteLine("UnRead: " + contactItem.WebPage);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static IEnumerable<T> readPst<T>(string pstFilePath) where T : class
        {
            List<T> mailItems = new List<T>();
            Application app = new Application();
            NameSpace outlookNs = app.GetNamespace("MAPI");
            // Add PST file (Outlook Data File) to Default Profile
            outlookNs.AddStore(pstFilePath);
            var stores = outlookNs.Stores.OfType<Store>().ToList();
            foreach (var store in stores.Where(x => x.FilePath == pstFilePath))
            {
                MAPIFolder rootFolder = store.GetRootFolder();
                // Traverse through all folders in the PST file
                // TODO: This is not recursive, refactor
                Folders subFolders = rootFolder.Folders;
                foreach (Folder folder in subFolders)
                {
                    RecursiveFolder<T>(mailItems, folder);
                }
                // Remove PST file from Default Profile
                try
                {
                    outlookNs.RemoveStore(rootFolder);
                }
                catch { }
            }
            return mailItems;
        }

        private static void RecursiveFolder<T>(List<T> mailItems, Folder folder) where T : class
        {
            Items items = folder.Items;
            foreach (var mailItem in items.OfType<T>())
            {
                mailItems.Add(mailItem);
            }
            foreach (var sub in folder.Folders.OfType<Folder>())
            {
                RecursiveFolder<T>(mailItems, sub);
            }
        }
    }
}